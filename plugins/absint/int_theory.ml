open Why3
open Term


let zero_int = t_const Number.(ConstInt ({ic_negative = false; ic_abs = int_literal_dec "0"})) Ty.ty_int
let one_int = t_const Number.(ConstInt ({ic_negative = false; ic_abs = int_literal_dec "1"})) Ty.ty_int
